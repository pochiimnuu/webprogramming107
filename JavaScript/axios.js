function init(){
  var div1 = document.getElementById('div1');
}

function fetch1(){
  fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(json => {console.log(json);
                 div1.innerHTML = "title: " + json[0].title});
}
function axios1(){
  axios.get('https://jsonplaceholder.typicode.com/posts')
  .then(response => console.log(response.data[0]))
  .catch(error=>console.log(error));
}

function post1(){
  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: 'foo',
      body: 'bar',
      userId: 1
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => console.log(json))
}
function axiosPost(){
   axios.post('https://jsonplaceholder.typicode.com/posts',{
     title:'foo',
     body: 'bar',
     userId: 1
   })
   .then(response=>console.log(response));
}
function axiosG1(){
   axios({
     method: 'post',
     url: 'https://jsonplaceholder.typicode.com/posts',
     data: {
       title: 'foo',
       body: 'I like 聯合大學',
       userId: 1
     }
   })
   .then(resp => console.log(resp))
   .catch(err => console.log(err))
}
