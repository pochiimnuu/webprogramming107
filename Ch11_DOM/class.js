var Person = function () {};
var p1 = new Person();
var p2 = new Person();
var Ball = function (x, y){
  this.x = x; this.y = y;
}
var b1 = new Ball(3, 10);
var b2 = new Ball(Math.random()%window.width, 100);
console.log(b1.x+", "+b1.y);
Ball.prototype.move = function (dx, dy){
  this.x += dx;
  this.y += dy;
  console.log(this.x + ", " + this.y);
}
b1.move(5, 1);
console.log(b1.x+", "+b1.y);
var fx = b1.move;
fx.call(3, 5);
alert("OK");
  console.log(b1.x+", "+b1.y);
